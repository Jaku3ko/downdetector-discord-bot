package com.plexclient.DownDetector.Utils;

import java.awt.*;
import java.util.Random;

public class ColorUtil {

    public static Color randomColor() {
        Random colorpicker = new Random();
        int red = colorpicker.nextInt(255) + 1;
        int green = colorpicker.nextInt(255) + 1;
        int blue = colorpicker.nextInt(255) + 1;
        return new Color(red, green, blue);
    }

}
