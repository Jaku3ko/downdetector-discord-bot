package com.plexclient.DownDetector;

import com.plexclient.DownDetector.Commands.Help;
import com.plexclient.DownDetector.Commands.Minecraft;
import com.plexclient.DownDetector.Commands.Skid;
import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.entities.Game;

import javax.security.auth.login.LoginException;

/**
 * @author jakuubkoo
 * @since 08/07/2019
 */
public class Main {

    private static JDA jda;

    public static void main(String[] args) throws LoginException, InterruptedException {
        jda = new JDABuilder(AccountType.BOT)
                .setToken("")
                .setGame(Game.listening("!helpstatus"))
                .buildBlocking();

        jda.addEventListener(new Skid());
        jda.addEventListener(new Minecraft());
        jda.addEventListener(new Help());
    }

}
