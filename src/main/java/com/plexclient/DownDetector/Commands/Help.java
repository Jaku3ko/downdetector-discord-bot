package com.plexclient.DownDetector.Commands;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import java.awt.*;
import java.io.IOException;

/**
 * @author jakuubkoo
 * @since 08/07/2019
 */
@SuppressWarnings("Duplicates")
public class Help extends ListenerAdapter {

    public void onGuildMessageReceived(GuildMessageReceivedEvent e) {
        String[] args = e.getMessage().getContentRaw().split(" ");

        EmbedBuilder eb = new EmbedBuilder();

        if (args[0].equalsIgnoreCase("!helpstatus")) {
            if (!e.getChannel().getId().equalsIgnoreCase("571436222040506368")) {
                if (!e.getChannel().getId().equalsIgnoreCase("593021702376652801")) {
                    eb.setTitle("Tried to execute help", null);
                    eb.setAuthor(e.getAuthor().getName(), e.getAuthor().getAvatarUrl(), e.getAuthor().getAvatarUrl());
                    eb.setColor(Color.red);
                    eb.setDescription("This command is only allowed at #bot-commands");
                    eb.setFooter("Powered by jakuubkoo with ❤", "https://i.imgur.com/4JtV2Ll.png");
                    e.getChannel().sendMessage(eb.build()).queue();
                    return;
                }
            }
            eb.setTitle("Help of Status commands", null);
            eb.setAuthor(e.getAuthor().getName(), e.getAuthor().getAvatarUrl(), e.getAuthor().getAvatarUrl());
            eb.setColor(Color.GREEN);
            eb.addField("", ":regional_indicator_h: :regional_indicator_e: :regional_indicator_l: :regional_indicator_p:status \u2B9E Main command for get help", false);
            eb.addField("", ":regional_indicator_m: :regional_indicator_c:status \u2B9E Get status of Minecraft servers", false);
            eb.addField("", ":regional_indicator_s: :regional_indicator_k: :regional_indicator_i: :regional_indicator_d:status \u2B9E Get status of Skid server", false);
            eb.setFooter("Powered by jakuubkoo with ❤", "https://i.imgur.com/4JtV2Ll.png");
            e.getChannel().sendMessage(eb.build()).queue();
        }
    }
}
