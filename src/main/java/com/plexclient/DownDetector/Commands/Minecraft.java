package com.plexclient.DownDetector.Commands;

import com.plexclient.DownDetector.Utils.ColorUtil;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.util.HashMap;

/**
 * @author jakuubkoo
 * @since 08/07/2019
 */
public class Minecraft extends ListenerAdapter {

    public void onGuildMessageReceived(GuildMessageReceivedEvent e) {
        String[] args = e.getMessage().getContentRaw().split(" ");

        EmbedBuilder eb = new EmbedBuilder();

        if (args[0].equalsIgnoreCase("!mcstatus")) {
            if (!e.getChannel().getId().equalsIgnoreCase("571436222040506368")) {
                if (!e.getChannel().getId().equalsIgnoreCase("593021702376652801")) {
                    eb.setTitle("Tried to execute Minecraft status", null);
                    eb.setAuthor(e.getAuthor().getName(), e.getAuthor().getAvatarUrl(), e.getAuthor().getAvatarUrl());
                    eb.setColor(Color.red);
                    eb.setDescription("This command is only allowed at #bot-commands");
                    eb.setFooter("Powered by jakuubkoo with ❤", "https://i.imgur.com/4JtV2Ll.png");
                    e.getChannel().sendMessage(eb.build()).queue();
                    return;
                }
            }
            eb.setTitle("Status of Minecraft Servers", null);
            eb.setAuthor(e.getAuthor().getName(), e.getAuthor().getAvatarUrl(), e.getAuthor().getAvatarUrl());
            eb.setAuthor(e.getAuthor().getName() + " executed command Minecraft status", e.getAuthor().getAvatarUrl());

            eb.setColor(ColorUtil.randomColor());

            /*
             * Minecraft
             * */
            if (pingURL("https://www.minecraft.net/", 1000)) {
                eb.addField("", ":white_check_mark: \u2B9E Minecraft.net: **This service is healthy.**", false);
            }else if(!pingURL("https://www.minecraft.net/", 1000)){
                eb.addField("", ":x: \u2B9E Minecraft.net: **This service is down.**", false);
            }

            /*
             * Account
             * */
            if (pingURL("http://Account.mojang.com/", 1000)) {
                eb.addField("", ":white_check_mark: \u2B9E Account.mojang.com: **This service is healthy.**", false);
            }else if(!pingURL("http://Account.mojang.com/", 1000)){
                eb.addField("",  ":x: \u2B9E Account.mojang.com: **This service is down.**", false);
            }

            /*
             * Auth server
             * */
            if (pingURL("http://Authserver.mojang.com/", 1000)) {
                eb.addField("", ":white_check_mark: \u2B9E Authserver.mojang.com: **This service is healthy.**", false);
            }else if(!pingURL("http://Authserver.mojang.com/", 1000)){
                eb.addField("", ":x: \u2B9E Authserver.mojang.com: **This service is down.**", false);
            }

            /*
             * Session server
             * */
            if (pingURL("http://Sessionserver.mojang.com/", 1000)) {
                eb.addField("", ":white_check_mark: \u2B9E Sessionserver.mojang.com: **This service is healthy.**", false);
            }else if(!pingURL("http://Sessionserver.mojang.com/", 1000)){
                eb.addField("", ":x: \u2B9E Sessionserver.mojang.com: **This service is down.**", false);
            }

            /*
             * Api server
             * */
            if (pingURL("http://Api.mojang.com/", 1000)) {
                eb.addField("", ":white_check_mark: \u2B9E Api.mojang.com: **This service is healthy.**", false);
            }else if(!pingURL("http://Api.mojang.com/", 1000)){
                eb.addField("", ":x: \u2B9E Api.mojang.com: **This service is down.**", false);
            }

            /*
             * Textures
             * */
            if (pingURL("http://Textures.minecraft.net/", 1000)) {
                eb.addField("", ":white_check_mark: \u2B9E Textures.minecraft.net: **This service is healthy.**", false);
            }else if(!pingURL("http://Textures.minecraft.net/", 1000)){
                eb.addField("", ":x: \u2B9E Textures.minecraft.net: **This service is down.**", false);
            }

            /*
             * Mojang
             * */
            if (pingURL("http://Mojang.com/", 1000)) {
                eb.addField("", ":white_check_mark: \u2B9E Mojang.com: **This service is healthy.**", false);
            }else if(!pingURL("http://Mojang.com/", 1000)){
                eb.addField("", ":x: \u2B9E Mojang.com: **This service is down.**", false);
            }

            eb.addBlankField(true);

            eb.setFooter("Powered by jakuubkoo with ❤", "https://i.imgur.com/4JtV2Ll.png");
            e.getChannel().sendMessage(eb.build()).queue();
        }
    }

    private boolean pingURL(String url, int timeout) {
        url = url.replaceFirst("^https", "http");

        try {
            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            connection.setConnectTimeout(timeout);
            connection.setReadTimeout(timeout);
            connection.setRequestMethod("HEAD");
            int responseCode = connection.getResponseCode();
            return (200 <= responseCode && responseCode <= 399);
        } catch (IOException exception) {
            return false;
        }
    }


}
