package com.plexclient.DownDetector.Commands;

import com.plexclient.DownDetector.Utils.ColorUtil;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;

/**
 * @author jakuubkoo
 * @since 08/07/2019
 */
public class Skid extends ListenerAdapter {

    public void onGuildMessageReceived(GuildMessageReceivedEvent e) {
        String[] args = e.getMessage().getContentRaw().split(" ");

        EmbedBuilder eb = new EmbedBuilder();

        if (args[0].equalsIgnoreCase("!skidstatus")) {
            if (!e.getChannel().getId().equalsIgnoreCase("571436222040506368")) {
                if (!e.getChannel().getId().equalsIgnoreCase("593021702376652801")) {
                    eb.setTitle("Tried to execute skidstatus", null);
                    eb.setAuthor(e.getAuthor().getName(), e.getAuthor().getAvatarUrl(), e.getAuthor().getAvatarUrl());
                    eb.setColor(Color.red);
                    eb.setDescription("This command is only allowed at #bot-commands");
                    eb.setFooter("Powered by jakuubkoo with ❤", "https://i.imgur.com/4JtV2Ll.png");
                    e.getChannel().sendMessage(eb.build()).queue();
                    return;
                }
            }
            try {
                if (getUserMap().isEmpty()) {
                    eb.setTitle("Executed command for status of Skid Server", null);
                    eb.setAuthor(e.getAuthor().getName(), e.getAuthor().getAvatarUrl(), e.getAuthor().getAvatarUrl());
                    eb.setAuthor(e.getAuthor().getName() + " executed command status", e.getAuthor().getAvatarUrl());
                    eb.setColor(Color.red);
                    eb.addField(":x: **This service is Down.**", "Please wait for LiquidDev", false);
                    eb.setFooter("Powered by jakuubkoo with ❤", "https://i.imgur.com/4JtV2Ll.png");
                    e.getChannel().sendMessage(eb.build()).queue();
                } else {
                    eb.setTitle("Executed command for status of Skid Server", null);
                    eb.setAuthor(e.getAuthor().getName(), e.getAuthor().getAvatarUrl(), e.getAuthor().getAvatarUrl());
                    eb.setColor(Color.GREEN);
                    eb.addField(":white_check_mark: **This service is UP.**", "You can play!", false);
                    eb.setFooter("Powered by jakuubkoo with ❤", "https://i.imgur.com/4JtV2Ll.png");
                    e.getChannel().sendMessage(eb.build()).queue();
                }
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }

    private HashMap<String, String> getUserMap() throws IOException {
        final URL url = new URL("");
        final URLConnection connect = url.openConnection();
        final InputStream instr = connect.getInputStream();
        final BufferedReader reader = new BufferedReader(new InputStreamReader(instr));
        final HashMap<String, String> players = new HashMap<String, String>();
        String line;
        while ((line = reader.readLine()) != null) {
            final String[] args = line.split(":");
            if (args.length == 2) {
                players.put(args[0], args[1]);
            }
        }
        reader.close();
        instr.close();
        return players;
    }

}
